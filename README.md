wikipedia-to-elasticsearch
==========================

[은전한닢 프로젝트](http://eunjeon.blogspot.kr)\([mecab-ko-dic](https://bitbucket.org/eunjeon/mecab-ko-dic), [mecab-ko-lucene-analyzer](https://bitbucket.org/eunjeon/mecab-ko-lucene-analyzer)\) 테스트를 위해서 위키피디아 데이터를 [Elasticsearch](https://www.elastic.co/products/elasticsearch)에 인덱싱하는 스크립트.

## 실행 방법

Elasticsearch에 [mecab-ko-lucene-analyzer](https://bitbucket.org/eunjeon/mecab-ko-lucene-analyzer)를 설치한 후, wikipedia_to_elasticsearch.py main 함수에서 다음의 코드를 상황에 맞게 수정하여 실행

    es_hosts = ({"host": "localhost", "port": 9200},)   # ElasticSearch 정보
    wikipedia_data_file = '../data/sample.xml'          # Wikipedia 데이터 파일 위치
