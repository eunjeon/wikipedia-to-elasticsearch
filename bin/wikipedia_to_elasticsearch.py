# -*- coding: utf-8 -*-
from datetime import datetime
from elasticsearch import Elasticsearch

__author__ = 'Yongwoon Lee <bibreen@gmail.com>'


class WikipediaDataParser():
    """
    wikipedia-extractor로 추출하는 Wikipedia 데이터가 well-formed xml이 아니라, 간단한 파서를 그냥 만듦
    Wikipedia 데이터 추출 프로그램은 https://github.com/bwbaugh/wikipedia-extractor 참조
    """
    IDLE = 0
    IN_DOC = 1

    def __init__(self, elastic_search):
        self.id = None
        self.title = ''
        self.title_and_contents = ''
        self.es = elastic_search
        self.count = 0
        self.state = self.IDLE

    def parse(self, file):
        while True:
            line = file.readline()
            if not line:
                break
            if self.state == self.IDLE:
                if line[0:5] == '<doc ':
                    attrs = self.get_attrs(line.strip())
                    self.start_element(attrs)
                    self.state = self.IN_DOC
                continue
            if self.state == self.IN_DOC:
                if line.strip() == '</doc>':
                    self.end_element()
                    self.state = self.IDLE
                else:
                    self.title_and_contents += line

    @staticmethod
    def get_attrs(tag):
        attrs = {}
        s = tag.strip('<>')
        s = s.split(' ', 1)[1]
        toks = s.split('" ')
        # print(toks)
        for tok in toks:
            d = tok.split('="')
            key = d[0]
            value = d[1].strip(' "')
            attrs[key] = value
        return attrs

    def start_element(self, attrs):
        self.id = attrs['id']
        self.title = attrs['title']
        self.title_and_contents = ''

    def end_element(self):
        # print('id: ' + self.id)
        # print('title: ' + self.title)
        # print('title_and_contents: ' + self.title_and_contents.strip('\n'))
        data = {
            'id': self.id,
            'title': self.title,
            'title_and_contents': self.title_and_contents.strip('\n')
        }
        self.es.index(index="wikipedia", doc_type="article", id=self.id, body=data)
        self.count += 1
        if self.count % 1000 == 0:
            print(str(self.count) + ' articles are indexed.')


def main():
    es_hosts = ({"host": "localhost", "port": 9200},)
    wikipedia_data_file = '../data/sample.xml'
    tokenizer_name = 'mecab_ko_standard_tokenizer'
    # wikipedia_data_file = '../data/kowiki-20150807-text.xml'
    # tokenizer_name = 'standard'

    es = Elasticsearch(hosts=es_hosts)
    es.indices.create(index='wikipedia', body=index_config(tokenizer_name), ignore=400)

    begin = datetime.now()
    parser = WikipediaDataParser(es)
    data_file = open(wikipedia_data_file, 'r')
    parser.parse(data_file)
    end = datetime.now()
    print(str(end - begin) + ' is elapsed.')
    data_file.close()


def index_config(tokenizer_name):
    return {
        "settings": {
            "index": {
                "analysis": {
                    "analyzer": {
                        "default": {
                            "type": "custom",
                            "tokenizer": tokenizer_name
                        }
                    }
                }
            }
        },
        "mappings": {
            "wikipedia": {
                "properties": {
                    "id": {"type": "string", "store": True, "index": "not_analyzed"},
                    "title": {"type": "string", "store": True, "term_vector": "yes"},
                    "title_and_contents": {"type": "string", "store": True, "term_vector": "yes"},
                },
                "_id": {"path": "id"},
                "_timestamp": {"enabled": True, "store": True},
            }
        }
    }


if __name__ == '__main__':
    main()
